# 20231017-SumanthPammi-NYCSchools


### Features

1) Home screen displaying all the schools data
2) Supports for iPhone X
3) Used MVVM
4) Covered Unit test cases
5) Compatibility with Light and Dark mode
6) Pods Integration for MBProgressHUD and OHHTTPStubs
