//
//  Notifications.swift
//  Sumanth-NYCSchools
//
//  Created by Sumanth Prakash Pammi on 10/17/23.
//

import Foundation

extension Notification.Name {
    static let flagsChanged = Notification.Name("FlagsChanged")
}
