//
//  UIViewController+Alert.swift
//  Sumanth-NYCSchools
//
//  Created by Sumanth Prakash Pammi on 10/17/23.
//

import UIKit

extension UIViewController {
    
    /**
     This is a Extention Method user for displying alert in Any View contoller
     - Parameters:
     - title: alert title as string
     - message: alert message as string
     */
    
    func showAlertMessageWith(title:String, message:String) {
        
        // create the alert
        let alert = UIAlertController(title: title, message:message, preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
}
