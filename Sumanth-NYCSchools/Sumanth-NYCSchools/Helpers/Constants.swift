//
//  Constants.swift
//  Sumanth-NYCSchools
//
//  Created by Sumanth Prakash Pammi on 10/17/23.
//

import Foundation

//MARK: - Constants

/// This struct has url and end points and stored in static variables. Developer can be access through out the application.

struct Constants {
    static let baseURL = "https://data.cityofnewyork.us/resource/"
    static let gethSchools = "s3k6-pzi2.json"
    static let getSATScores = "f9bf-2cp4.json"
}

//MARK: - APIError

/// Created enum to handle APIError -  It gives the information about the each end point.

enum APIError: Error,Equatable {
   
    case schoolAPIError
    case satAPIError
    case unexpected(code: Int)
    
    public var description: String {
        switch self {
        case .schoolAPIError:
            return NSLocalizedString(
                "Error while fetching schools API",
                comment: "Schools API Error"
            )
        case .satAPIError:
            return NSLocalizedString(
                "Error while fetching SAT API",
                comment: "SAT API Error"
            )
        case .unexpected(_):
            return NSLocalizedString(
                "An unexpected error occurred.",
                comment: "Unexpected Error"
            )
        }
    }
}
