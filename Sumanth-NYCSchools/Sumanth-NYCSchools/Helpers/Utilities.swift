//
//  Utilities.swift
//  Sumanth-NYCSchools
//
//  Created by Sumanth Prakash Pammi on 10/17/23.
//

import Foundation

struct Utilites {
    
    /**
     - type: Generic type of class
     - from: response as data format
     - completion: returns the data moel or error
     */
    
    static func convertResponseToModel<T>(type: T.Type, from data: Data, completion:@escaping((T?, Error?) -> Void)) where T : Decodable {
        do{
            let dataModel = try JSONDecoder().decode(type, from: data)
            completion(dataModel,nil)
        }
        catch DecodingError.dataCorrupted(let context){
            completion(nil, context.underlyingError)
        }
        catch let err {
            completion(nil, err)
        }
    }
}
