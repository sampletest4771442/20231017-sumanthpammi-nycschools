//
//  SchoolViewModel.swift
//  Sumanth-NYCSchools
//
//  Created by Sumanth Prakash Pammi on 10/17/23.
//

import Foundation

class SchoolViewModel {
    
    /// Store all schools data
    fileprivate var rawSchoolsData = [SchoolModel]()
    fileprivate var webService = Webservices()
    var schoolSATData: SchoolsSATData = []
    
    var schoolsData:SchoolsData {
        if searchString.isEmpty {
            return rawSchoolsData
        } else {
            let filteredSchoolsData = rawSchoolsData.filter { (school) -> Bool in
                return school.schoolName?.containsIgnoringCase(find: searchString) ?? false
            }
            return filteredSchoolsData
        }
    }
    
    fileprivate(set) var searchString = ""

    /// Fetch Schools and SAT API
    /// - Parameter completion: completion handler with Error
    
    func fetchSchoolsData(completion:@escaping((APIError?) -> Void)) {
        
        let group = DispatchGroup()
        var schoolListError: APIError? = nil
        var satScoresError: APIError? = nil
        group.enter()
        
        getSchoolsList { error in
            schoolListError = error
            group.leave()
        }
        group.enter()
        getSchoolSaTData { error in
            satScoresError = error
            group.leave()
        }
        
        group.notify(queue: .main)  {
            
            guard schoolListError == nil, satScoresError == nil else {
                completion(schoolListError ?? satScoresError)
                return
            }
            completion(nil)
        }
    }

    /// Get schools API call
    /// - Parameter completion: returns error with optional
    
    func getSchoolsList(completion: @escaping (_ error:APIError?) -> Void) {
        Webservices().fetchSchoolsDataFromAPI { schoolData, error in
            if error == nil {
                self.rawSchoolsData = schoolData ?? []
                completion(nil)
            } else {
                completion(APIError.schoolAPIError)
            }
        }
    }
    

    func searchSchool(string: String) {
        searchString = string
    }
    
    /// Get SAT API call
    /// - Parameter completion:  returns error with optional
    ///
    func getSchoolSaTData(completion: @escaping (_ error:APIError?) -> Void) {
        Webservices().fetchSchoolsSATDataFromAPI { satData, error in
            if error == nil {
                self.schoolSATData = satData ?? []
                completion(nil)
            } else {
                completion(APIError.satAPIError)
            }
        }
    }

    /// Filter SAT data from schools data
    /// - Parameter uniqueid: String value. ex: dbn
    /// - Returns: value returns after filter

    func getSATDataWith(uniqueid: String) -> SchoolSATDataModel? {
        
        return schoolSATData.first { schoolDataModel in
            schoolDataModel.dbn == uniqueid
        }
    }
}
