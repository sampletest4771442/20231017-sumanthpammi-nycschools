//
//  ViewController.swift
//  Sumanth-NYCSchools
//
//  Created by Sumanth Prakash Pammi on 10/17/23.
//

import UIKit
import MBProgressHUD

class ViewController: UIViewController, Storyboarded {
    
    //MARK: - outlets
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblSchools: UITableView!
    private var schoolViewModel = SchoolViewModel()

    //MARK: - viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.title = "NYC Schools"
        prepareSchoolData()
    }
    
    //MARK: - prepareSchoolData()
    
    private func prepareSchoolData() {
        // Show HUD
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.label.text = "Loading..."
        schoolViewModel.fetchSchoolsData(completion: { [weak self] error in
            //Hide Hud
            progressHUD.hide(animated: true)
            if case .schoolAPIError = error {
                //Display Schools API failure error
                self?.showAlertMessageWith(title: "Error", message: error?.description ?? "Error while fetching API")
                return
            }
            self?.tblSchools.reloadData()
        })
    }
}



extension ViewController: UITableViewDataSource {
    
    //MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolViewModel.schoolsData.count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolTableCell") as? SchoolTableCell else { return UITableViewCell() }
        cell.school = schoolViewModel.schoolsData[indexPath.row]
        
        return cell
    }
}

extension ViewController: UITableViewDelegate {
    
    //MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didSelectRow(index: indexPath.row)
    }
    
    private func didSelectRow(index : Int) {
        let school = schoolViewModel.schoolsData[index]
        let schoolSATDetails = schoolViewModel.getSATDataWith(uniqueid: school.dbn ?? "")
        let dataModel = SATDataDisplayModel()
        dataModel.schoolLatitute = school.latitude
        dataModel.schoolLongitute = school.longitude
        dataModel.schoolOverView = school.overviewParagraph
        dataModel.phoneNumber = school.phoneNumber
        guard let navi = self.navigationController else { return }
        let coordinator = MainCoordinator(navigationController: navi)
        coordinator.schoolDetails(schoolName : school.schoolName, satDataModel: schoolSATDetails, satDetailModel: dataModel)
    }
}

extension ViewController: UISearchBarDelegate {
    
    //MARK: - UISearchBarDelegates
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text != schoolViewModel.searchString {
            schoolViewModel.searchSchool(string: searchBar.text ?? "")
            tblSchools.reloadData()
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = schoolViewModel.searchString
        searchBar.resignFirstResponder()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
