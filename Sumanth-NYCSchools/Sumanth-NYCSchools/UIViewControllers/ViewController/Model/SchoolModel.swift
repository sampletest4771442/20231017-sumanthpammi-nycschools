//
//  SchoolModel.swift
//  Sumanth-NYCSchools
//
//  Created by Sumanth Prakash Pammi on 10/17/23.
//

import Foundation

// MARK: - SchoolsData

struct SchoolModel: Codable {
    let dbn, schoolName, overviewParagraph, languageClasses: String?
    let location, phoneNumber, schoolEmail, website: String?
    let primaryAddressLine1, city, zip, stateCode: String?
    let latitude, longitude: String?

    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case overviewParagraph = "overview_paragraph"
        case languageClasses = "language_classes"
        case location
        case phoneNumber = "phone_number"
        case schoolEmail = "school_email"
        case website
        case primaryAddressLine1 = "primary_address_line_1"
        case city, zip
        case stateCode = "state_code"
        case latitude, longitude
    }
}

typealias SchoolsData = [SchoolModel]
