//
//  SchoolSATDataModel.swift
//  Sumanth-NYCSchools
//
//  Created by Sumanth Prakash Pammi on 10/17/23.
//

import Foundation

// MARK: - SchoolsScoreSAT

struct SchoolSATDataModel: Codable {
    let dbn, schoolName, numOfSatTestTakers, satCriticalReadingAvgScore: String?
    let satMathAvgScore, satWritingAvgScore: String?

    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case numOfSatTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
    }
}

typealias SchoolsSATData = [SchoolSATDataModel]


class SATDataDisplayModel {
    var schoolLatitute: String? = nil
    var schoolLongitute: String? = nil
    var phoneNumber:String? = nil
    var schoolOverView:String? = nil
    
}
