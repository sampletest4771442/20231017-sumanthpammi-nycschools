//
//  DetailsViewController.swift
//  Sumanth-NYCSchools
//
//  Created by Sumanth Prakash Pammi on 10/17/23.
//

import UIKit
import MapKit

class DetailsViewController: UIViewController, Storyboarded {

    //MARK: - outlets
    
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var readingSATScoreLabel: UILabel!
    @IBOutlet weak var mathSATScoreLabel: UILabel!
    @IBOutlet weak var writingLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var schoolSATModel:SchoolSATDataModel?
    var satDataDisplayModel:SATDataDisplayModel?
    var schoolName : String?
    
    //MARK: - viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        title = "Details"
        scrollView.isScrollEnabled = false
        prepareData()
    }
    
    //MARK: - viewDidAppear
    
    override func viewDidAppear(_ animated: Bool) {
        addAnnotation()
    }
    
    //MARK: - prepareData
    
   private func prepareData(){
       schoolNameLabel.text = schoolName ?? ""
       guard let data = schoolSATModel else { return }
       scrollView.isScrollEnabled = true
       schoolNameLabel.text = data.schoolName
       let reading = "SAT Average Reading Score : " + (data.satCriticalReadingAvgScore ?? "NA")
       let writing = "SAT Average Writing Score : " + (data.satWritingAvgScore ?? "NA")
       let mathScore = "SAT Average Maths Score : " + (data.satMathAvgScore ?? "NA")
       let overview = "Overview : \n \t" + (satDataDisplayModel?.schoolOverView ?? "")
       let phoneNumber = "Phone Number : " + (satDataDisplayModel?.phoneNumber ?? "NA")
       
       readingSATScoreLabel.attributedText(withString: reading, boldString: "\(data.satCriticalReadingAvgScore ?? "")")
       writingLabel.attributedText(withString: writing, boldString: "\(data.satWritingAvgScore ?? "")")
       mathSATScoreLabel.attributedText(withString: mathScore, boldString: "\(data.satMathAvgScore ?? "")")
       overviewLabel.attributedText(withString: overview, boldString: "Overview : \n")
       phoneNumberLabel.attributedText(withString: phoneNumber, boldString: "\(satDataDisplayModel?.phoneNumber ?? "")")

    }
    
    
    //MARK: - addAnnotation
    
    /*
      This method is displaying scholl location on the map.
     */
    
    private func addAnnotation() {
        
        guard let latitute = satDataDisplayModel?.schoolLatitute, let longitute = satDataDisplayModel?.schoolLongitute else {
            return
        }
        let annotation = MKPointAnnotation()
        annotation.title = schoolSATModel?.schoolName ?? ""
        annotation.coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(Double(latitute) ?? 0.0), longitude: CLLocationDegrees(Double(longitute) ?? 0.0))
        self.mapView.addAnnotation(annotation)
        mapView.centerCoordinate = annotation.coordinate;
    }
}
