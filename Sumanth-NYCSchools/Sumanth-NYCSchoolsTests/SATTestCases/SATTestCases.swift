//
//  SATTestCases.swift
//  Sumanth-NYCSchoolsTests
//
//  Created by Sumanth Prakash Pammi on 10/17/23.
//

import XCTest
@testable import Sumanth_NYCSchools
import OHHTTPStubs

final class SATTestCases: BaseTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
       try super.setUpWithError()
        

    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
     
     func testSATSchoolsAPI() {
         
         addStub(path: schoolsSatAPIPath, responseJsonFile: satAPIResponse, error: nil)

         let expectation = XCTestExpectation(description: "Get SAT Data API")
         let viewModel = SchoolViewModel()
         viewModel.getSchoolSaTData { error in
             XCTAssertNil(error)
             expectation.fulfill()
         }
         wait(for: [expectation], timeout: kAPIRequestWaitTime)
     }
     
     func testSATAPIDataCount() {
         
         addStub(path: schoolsSatAPIPath, responseJsonFile: satAPIResponse, error: nil)

         let expectation = XCTestExpectation(description: "SAT Data API Count")
         let viewModel = SchoolViewModel()
         viewModel.getSchoolSaTData { error in
             XCTAssertTrue(viewModel.schoolSATData.count > 0)
             expectation.fulfill()
         }
         wait(for: [expectation], timeout: kAPIRequestWaitTime)
     }
     
     func testSATAPIError() {
         
         addStub(path: schoolsSatAPIPath, responseJsonFile: nil, error: NSError(domain: "SAT API Fail", code: 404, userInfo: nil))

         let expectation = XCTestExpectation(description: "Get Schools SAT Data API Fail")
         let viewModel = SchoolViewModel()
         viewModel.getSchoolSaTData { error in
             XCTAssert(error != nil)
             expectation.fulfill()
         }
         wait(for: [expectation], timeout: kAPIRequestWaitTime)
     }
}

