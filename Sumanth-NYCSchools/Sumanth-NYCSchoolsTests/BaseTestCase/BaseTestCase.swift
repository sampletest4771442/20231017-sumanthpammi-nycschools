//
//  BaseTestCase.swift
//  Sumanth-NYCSchoolsTests
//
//  Created by Sumanth Prakash Pammi on 10/17/23.
//

import XCTest
@testable import Sumanth_NYCSchools
import OHHTTPStubs

 class BaseTestCase: XCTestCase {

    func addStub(path: String, responseJsonFile: String?, error: Error?) {
        stub(condition: isPath(path)) { (request) -> HTTPStubsResponse in
            if let responseJsonFile = responseJsonFile {
                let stubPath  = OHPathForFile(responseJsonFile, type(of: self))
                return fixture(filePath: stubPath!, headers: ["Content-Type":"application/json"])
            } else if let error = error {
                return HTTPStubsResponse(error: error)
            }
            return HTTPStubsResponse(error: NSError(domain: "Testing Error", code: 404, userInfo: nil))
        }
    }
}
